# Android Network Security Configuration

- How to use the Volley library to make network requests.
- How to use a Network Security Configuration to help make network communication more secure.
- How to modify some advanced Network Security Configuration options that will help during development and testing.

